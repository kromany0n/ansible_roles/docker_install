- name: Assert that the users exists
  ansible.builtin.command: "id {{ item }}"
  loop: "{{ docker_install_add_users_to_docker_group }}"
  register: docker_install_assert_users_exist
  changed_when: docker_install_assert_users_exist.rc != 0

- name: Debian/Ubuntu install
  when: ansible_distribution in ["Debian", "Ubuntu"]
  block:
    - name: Ensure old packages are not installed
      ansible.builtin.apt:
        name:
          - docker.io
          - docker-doc
          - docker-compose
          - docker-compose-v2
          - podman-docker
          - containerd
          - runc
        state: absent

    - name: Install required packages
      ansible.builtin.apt:
        name:
          - ca-certificates
          - gpg-agent
        update_cache: true

    - name: Download repo key
      ansible.builtin.apt_key:
        url: "{{ docker_install_apt_key_url }}"

    - name: Add Docker repo
      ansible.builtin.apt_repository:
        repo: "{{ docker_install_apt_repository }}"
        state: present

    - name: Install docker packages
      ansible.builtin.apt:
        name:
          - docker-ce
          - docker-ce-cli
          - containerd.io
          - docker-buildx-plugin
          - docker-compose-plugin
        state: present
        update_cache: "{{ docker_install_apt_update_cache }}"
      notify:
        - Restart docker
        - Start docker

- name: RHEL-like install
  when: ansible_distribution in ["CentOS", "Rocky"]
  block:
    - name: Ensure old packages are not installed
      ansible.builtin.dnf:
        name:
          - docker
          - docker-client
          - docker-client-latest
          - docker-common
          - docker-latest
          - docker-latest-logrotate
          - docker-logrotate
          - docker-engine
        state: absent

    - name: Add yum repository
      ansible.builtin.get_url:
        url: "{{ docker_install_rhel_repository_url }}"
        dest: /etc/yum.repos.d/docker-ce.repo
        mode: "0644"

    - name: Install docker packages
      ansible.builtin.dnf:
        name:
          - docker-ce
          - docker-ce-cli
          - containerd.io
          - docker-buildx-plugin
          - docker-compose-plugin
        state: present
      notify:
        - Restart docker
        - Start docker

- name: Create docker.json file
  ansible.builtin.copy:
    dest: "{{ docker_install_daemon_config_path }}"
    mode: "0600"
    content: "{{ docker_install_daemon_options | to_json() }}"
  notify: Restart docker

- name: Add prune crontab
  when: docker_install_crontab_prune
  ansible.builtin.cron:
    name: docker prune
    special_time: daily
    job: "docker system prune -af --filter {{ docker_install_crontab_prune_filter }}"

- name: Add users to group "docker"
  ansible.builtin.user:
    groups:
      - docker
    append: true
    name: "{{ item }}"
  loop: "{{ docker_install_add_users_to_docker_group }}"
