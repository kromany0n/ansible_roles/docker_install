docker_install
=========

Утанавливает Docker

Requrements
-----------

Тестировалось молекулой на Debian 10,11,12, Ubuntu 20,22,24, Rocky 9

Example Playbook
----------------

```yaml
- name: Converge
  hosts: all
  gather_facts: true
  vars:
    docker_install_restart_docker: true
    docker_install_crontab_prune: true
    docker_install_add_users_to_docker_group:
      - user1
      - user2
    # docker_install_daemon_options:
  roles:
    - docker_install
```

License
-------

BSD
