import pytest

@pytest.mark.parametrize("name", [
    "docker-ce",
    "docker-ce-cli",
    "containerd.io",
    "docker-buildx-plugin",
    "docker-compose-plugin",
])
def test_debian_packages(host, name: str):
    if host.system_info.distribution in ["debian", "ubuntu"]:
        pkg = host.package(name)
        assert pkg.is_installed


@pytest.mark.parametrize("name", [
    "docker",
    "docker-client",
    "docker-client-latest",
    "docker-common",
    "docker-latest",
    "docker-latest-logrotate",
    "docker-logrotate",
    "docker-engine"
])
def test_rocky_packages(host, name):
    if host.system_info.distribution == "roky":
        pkg = host.package(name)
        assert pkg.is_installed

def test_docker_ps(host):
    ps = host.run("docker ps")
    assert ps.rc == 0

def test_cron(host):
    crontab = host.run("crontab -l | grep '@daily docker system prune -af --filter until=720h'")
    assert crontab.rc == 0

@pytest.mark.parametrize("user", ["dokeruser1", "dokeruser2"])
def test_users_group(host, user):
    assert "docker" in host.user(user).groups
